use std::{
    error,
    io::{stdout, Read, Write},
    thread::sleep,
    time::Duration,
};
use termion::{
    async_stdin, clear, cursor,
    event::{Event, Key},
    raw::IntoRawMode,
    screen::AlternateScreen,
    terminal_size,
};

mod event;
mod song;
mod structs;

mod draw;

use draw::library::Library;
use draw::queue::Queue;
use event::parse_event;

fn main() -> Result<(), Box<dyn error::Error>> {
    let port = "127.0.0.1:6600";
    let mut mpd = mpd::Client::connect(port)?;

    let mut mpd_status = structs::MpdStatus::new(mpd.currentsong()?, mpd.status()?, mpd.queue()?);

    let queue = Queue::new(mpd.queue().unwrap_or_default());

    let mut library = Library::new(
        mpd.list(
            &mpd::search::Term::Tag("Artist".into()),
            &mpd::search::Query::new(),
        )
        .unwrap_or_default(),
    );

    library.update_all(&mut mpd)?;
    let mut display = draw::display::Display::new(terminal_size()?, library, queue);

    let mut stdout = AlternateScreen::from(stdout().into_raw_mode()?);
    let mut stdin = async_stdin().bytes();

    write!(
        stdout,
        "{}",
        draw::draw(&mut mpd, &mut mpd_status, &mut display, true)?,
    )?;
    write!(stdout, "{}", cursor::Hide)?;

    let msg = 'main: loop {
        let resized = display.update(terminal_size()?);
        if resized && (display.size.cols < 70 || display.size.rows < 10) {
            break 'main "terminal too small".to_string();
        }

        let mut input = true;
        if let Some(Ok(b)) = stdin.next() {
            if let Ok(e) = parse_event(b, &mut stdin) {
                match e {
                    Event::Key(Key::Ctrl('q')) => break 'main "Bye".to_string(),
                    Event::Key(Key::Char('p')) => mpd.toggle_pause().unwrap_or_default(),
                    Event::Key(Key::Char('q')) => {
                        display.change_screen(draw::display::Screen::Queue)
                    }
                    Event::Key(Key::Char('w')) => {
                        display.change_screen(draw::display::Screen::Library)
                    }

                    // Event::Key(key::Char(c)) if c.is_digit(10) => display.modifier(c, &mut mpd),
                    Event::Key(e) => input = display.event(e, &mut mpd)?,

                    _ => input = false,
                }
            }
        } else {
            input = false
        }

        match draw::draw(&mut mpd, &mut mpd_status, &mut display, resized) {
            Ok(s) => write!(stdout, "{}", s)?,
            Err(e) => break 'main format!("{}", e),
        };

        stdout.flush()?;
        if !input {
            sleep(Duration::from_millis(10));
        }
    };

    write!(stdout, "{}{}", clear::All, cursor::Show)?;
    stdout.flush()?;
    drop(stdout);

    println!("{}", msg);

    Ok(())
}
