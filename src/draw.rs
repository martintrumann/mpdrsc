//! Used for generating strings that are ment to directly draw to the screen
use termion::clear;

mod utils;

pub mod display;
pub mod header;
pub mod library;
pub mod queue;

pub fn draw(
    mpd: &mut mpd::Client,
    mpd_status: &mut crate::structs::MpdStatus,
    display: &mut display::Display,
    resized: bool,
) -> Result<String, mpd::error::Error> {
    // update mpd_status
    mpd_status.update(mpd.currentsong()?, mpd.status()?, mpd.queue()?);

    let mut out = String::new();
    if resized {
        out += &format!("{}", clear::All);
    }

    out += &header::header(
        mpd_status,
        display,
        resized,
        mpd_status.song_changed,
        mpd_status.status_changed,
    );

    out += &display.draw(mpd, resized)?;

    Ok(out)
}
