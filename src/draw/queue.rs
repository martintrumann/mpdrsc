use termion::{color, style};

/// Possible queue actions
pub enum Action {
    MoveUp(usize),
    MoveDown(usize),
    RemoveSong(Option<usize>),
    Clear,
}

/// Main queue struct
pub struct Queue {
    i: Option<usize>,
    song: Option<mpd::Song>,
    pub queue: Vec<mpd::Song>,
    pub queue_changed: bool,
    changed: bool,
    clear: bool,
}
impl Queue {
    /// create new queue
    pub fn new(queue: Vec<mpd::Song>) -> Queue {
        Queue {
            i: None,
            song: None,
            queue,
            queue_changed: false,
            changed: false,
            clear: false,
        }
    }

    /// Ecevute Actions
    pub fn execute(
        &mut self,
        action: Action,
        mpd: &mut mpd::Client,
    ) -> Result<(), mpd::error::Error> {
        match action {
            Action::MoveUp(a) => {
                if let Some(i) = self.i {
                    if i != 0 {
                        if i >= a {
                            self.i = Some(i - a);
                        } else {
                            self.i = Some(0)
                        }
                    }
                }

                Ok(())
            }
            Action::MoveDown(a) => {
                if let Some(i) = self.i {
                    if i != self.queue.len() {
                        if i + a < self.queue.len() {
                            self.i = Some(i + a);
                        } else {
                            self.i = Some(self.queue.len())
                        }
                    }
                }

                Ok(())
            }
            Action::RemoveSong(i) => match i {
                Some(num) => {
                    if num < self.queue.len() {
                        mpd.delete(num as u32)
                    } else {
                        Ok(())
                    }
                }

                None => {
                    if let Some(i) = self.i {
                        mpd.delete(i as u32)
                    } else {
                        Ok(())
                    }
                }
            },
            Action::Clear => {
                self.clear = true;
                mpd.clear()
            }
        }
    }

    /// Update queue
    pub fn update(&mut self, mpd: &mut mpd::Client) -> Result<bool, mpd::error::Error> {
        let queue = mpd.queue()?;
        self.queue_changed = self.queue != queue;

        if self.queue_changed {
            self.queue = queue.clone();
        }

        if queue.is_empty() {
            self.i = None;
            self.song = None;

            self.changed = self.queue_changed;
            Ok(self.queue_changed)
        } else {
            if let Some(mut i) = self.i {
                if i > self.queue.len() - 1 {
                    i = self.queue.len() - 1;
                    self.i = Some(i);
                }

                if let Some(song) = &self.song {
                    if *song == self.queue[i] {
                        self.changed = false;
                        return Ok(false);
                    } else {
                        self.song = Some(self.queue[i].clone());
                        self.changed = true;
                    }
                } else {
                    self.song = Some(self.queue[i].clone());
                    self.changed = true;
                }
            } else {
                self.changed = true;
                self.i = Some(0);
                self.song = Some(self.queue[0].clone());
            }

            Ok(true)
        }
    }

    pub fn draw(
        &mut self,
        mpd: &mut mpd::Client,
        size: &mut super::display::Size,
        redraw_window: bool,
    ) -> Result<String, mpd::error::Error> {
        let mut out = String::new();

        if redraw_window {
            out += &format!(
                "{}{}{}{}{}{}{}{}",
                color::Fg(color::Yellow),
                super::utils::goto_print(1, 7, "─".repeat(size.cols as usize)),
                style::Bold,
                super::utils::goto_print(1, 6, "Title"),
                super::utils::goto_print(60, 6, "Artist"),
                color::Fg(color::Reset),
                super::utils::goto_print(size.half_cols - 2, 4, " Queue "),
                style::NoBold,
            );
        }

        if self.changed || self.queue_changed || redraw_window {
            if self.queue_changed {
                if self.clear {
                    for i in 0..size.rows - 8 {
                        out += &super::utils::goto_print(1, 8 + i, termion::clear::CurrentLine);
                    }
                } else {
                    out += &super::utils::goto_print(
                        1,
                        8 + self.queue.len() as u16,
                        termion::clear::CurrentLine,
                    );
                }
            }

            for (i, song) in self.queue.clone().into_iter().enumerate() {
                if i > size.rows as usize + 8 {
                    // TODO: impliment scrolling
                    break;
                }

                // current song will be bold
                if let Ok(Some(current_song)) = &mpd.currentsong() {
                    if song == *current_song {
                        out += &format!("{}", style::Bold);
                    }
                }

                if let Some(selected_song) = self.song.clone() {
                    if selected_song == song {
                        out += &format!("{}", style::Invert);
                    }
                }

                let name = crate::song::title_or_filename(&song);
                let artist = song.artist.unwrap_or_default();

                out += &format!(
                    "{}{}{}{}{}{}",
                    super::utils::goto_print(1, 8 + i as u16, name.clone()),
                    " ".repeat(59 - name.len()),
                    color::Fg(color::Yellow),
                    &artist.clone(),
                    " ".repeat(size.cols as usize - 59 - artist.len()),
                    color::Fg(color::Reset),
                );

                out += &format!("{}{}", style::NoInvert, style::NoBold);
            }
        }

        Ok(out)
    }
}

pub fn add_to_queue(
    song: mpd::Song,
    obj: String,
    mpd: &mut mpd::Client,
) -> Result<(), mpd::error::Error> {
    if obj == "song" {
        mpd.push(song)?;
    }

    Ok(())
}

pub fn add_to_queue_and_play(
    song: mpd::Song,
    obj: String,
    mpd: &mut mpd::Client,
) -> Result<(), mpd::error::Error> {
    if obj == "song" {
        let id = mpd.push(song)?;
        mpd.switch(id)?
    }

    Ok(())
}

pub fn song_in_queue(
    song: &mpd::Song,
    mpd: &mut mpd::Client,
) -> Result<Option<usize>, mpd::error::Error> {
    Ok(mpd.queue()?.iter().position(|s| s.file == song.file))
}

pub fn remove_song_from_queue(
    song: &mpd::Song,
    mpd: &mut mpd::Client,
) -> Result<bool, mpd::error::Error> {
    if let Some(n) = song_in_queue(song, mpd)? {
        mpd.delete(n as u32)?;

        Ok(true)
    } else {
        Ok(false)
    }
}
