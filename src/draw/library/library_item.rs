//! Item in library column

/// Item that goes into library
#[derive(Debug)]
pub enum LibraryItem {
    SongItem(SongItem),
    ArtistAlbumItem(ArtistAlbumItem),
}
impl LibraryItem {
    /// Get the items name
    pub fn name(&self) -> String {
        match self {
            Self::SongItem(s) => s.title_or_filename(),
            Self::ArtistAlbumItem(s) => s.name.clone(),
        }
    }

    /// Check if the item is currently selected
    pub fn selected(&self) -> bool {
        match self {
            Self::SongItem(s) => s.selected,
            Self::ArtistAlbumItem(s) => s.selected,
        }
    }

    /// Check if the items column is currently selected
    pub fn column_selected(&self) -> bool {
        match self {
            Self::SongItem(s) => s.column_selected,
            Self::ArtistAlbumItem(s) => s.column_selected,
        }
    }

    /// Check if Somg Item is in the queue
    pub fn in_queue(&self) -> Option<bool> {
        match self {
            Self::SongItem(s) => Some(s.in_queue),
            Self::ArtistAlbumItem(_) => None,
        }
    }
}

// Song {1
/// Song item in song Column
#[derive(Debug)]
pub struct SongItem {
    song: mpd::Song,
    selected: bool,
    in_queue: bool,
    column_selected: bool,
}
impl SongItem {
    /// Create new item
    pub fn new(song: mpd::Song, selected: bool, in_queue: bool, column_selected: bool) -> SongItem {
        SongItem {
            song,
            selected,
            in_queue,
            column_selected,
        }
    }

    /// Get the title or the filename of the song
    fn title_or_filename(&self) -> String {
        if let Some(title) = &self.song.title {
            title.to_string()
        } else if let Some(filename) = self.song.file.split('/').last() {
            String::from(filename)
        } else {
            String::from("")
        }
    }
}

// ArtistAlbumItem {1
/// Item in Artist or Album column
#[derive(Debug)]
pub struct ArtistAlbumItem {
    name: String,
    /// If the item is currently selected
    selected: bool,
    /// If the column is current selected
    column_selected: bool,
}
impl ArtistAlbumItem {
    /// Create multiple Items from a vector of strings
    pub fn from_vec(vec: Vec<String>, num: usize, column_selected: bool) -> Vec<LibraryItem> {
        let mut out_vec: Vec<LibraryItem> = Vec::new();

        for (i, name) in vec.into_iter().enumerate() {
            out_vec.push(LibraryItem::ArtistAlbumItem(ArtistAlbumItem {
                name,
                selected: (i == num),
                column_selected,
            }))
        }

        out_vec
    }
}
