//! Library screen
mod library_item;

use mpd::{Query, Term};
use termion::{color, style};

use super::utils::{clear_section, goto_print};

/// Width and starts of columns
#[derive(Default)]
struct DrawingColumns {
    /// Start of albums column
    album_start: u16,
    /// Start of songs column
    song_start: u16,
    /// Width of artists column
    artist_cols: u16,
    /// Width of albums column
    album_cols: u16,
    /// Width of songs column
    song_cols: u16,
}
impl DrawingColumns {
    /// Resize columns
    pub fn update(&mut self, size: &super::display::Size) {
        if size.cols / 3 != self.album_start {
            self.album_start = size.cols / 3;
            self.song_start = (size.cols / 3) * 2;

            self.artist_cols = self.album_start - 1;
            self.album_cols = self.song_start - self.album_start;
            self.song_cols = size.cols - self.song_start;
        }
    }
}

/// All library columns
#[derive(PartialEq)]
enum Column {
    Artists,
    Albums,
    Songs,
}
impl Default for Column {
    fn default() -> Column {
        Column::Artists
    }
}

/// All possible Library Actions
#[allow(dead_code)]
pub enum Action {
    /// Move the cursor up by number of items
    MoveUp(usize),
    /// Move the cursor down by number of items
    MoveDown(usize),
    /// Scroll the cursor up by number of items
    ScrollUp(usize),
    /// Scroll the cursor down by number of items
    ScrollDown(usize),
    /// Move the cursor to the previous column
    MoveLeft,
    /// Move the cursor to the next column
    MoveRight,
    /// Add of remove the current song from play queue
    AddOrRemoveFromQueue,
    /// remove the current song from play queue
    RemoveFromQueue,
    /// Add the current song to play queue
    AddToQueue,
    /// Add the current song to play queue and play it
    AddToQueueAndPlay,
}

/// Main Library struct
pub struct Library {
    /// Start and width of columns
    cols: DrawingColumns,

    artist_i: usize,
    artist_scroll: usize,
    artists: Vec<String>,
    artist: String,

    album_i: usize,
    album_scroll: usize,
    albums: Vec<String>,
    album: String,

    song_i: usize,
    song_scroll: usize,
    songs: Vec<mpd::Song>,
    song: mpd::Song,

    /// Current column
    column: Column,
    /// How long does column sty hidden after previous column changes
    column_timeout: u8,
    /// Counter for column hiding
    counter: u8,
    /// Check if column changed last frame
    column_changed: bool,
}
impl Library {
    /// Create new library
    pub fn new(artists: Vec<String>) -> Library {
        Library {
            cols: DrawingColumns::default(),

            artist_i: 0,
            artist_scroll: 0,
            artists,
            artist: String::new(),

            album_i: 0,
            album_scroll: 0,
            albums: Vec::new(),
            album: String::new(),

            song_i: 0,
            song_scroll: 0,
            songs: Vec::new(),
            song: mpd::Song::default(),

            column: Column::Artists,
            counter: 0,
            column_timeout: 10,
            column_changed: false,
        }
    }

    /// Execute an action on library
    pub fn execute(
        &mut self,
        action: Action,
        mpd: &mut mpd::Client,
    ) -> Result<(), mpd::error::Error> {
        match action {
            Action::ScrollUp(a) => {
                self.column_changed = true;
                match self.column {
                    Column::Artists => {
                        if self.artist_scroll >= a {
                            self.artist_scroll -= a
                        } else {
                            self.artist_scroll = 0
                        }
                    }
                    Column::Albums => {
                        if self.album_scroll >= a {
                            self.album_scroll -= a
                        } else {
                            self.album_scroll = 0
                        }
                    }
                    Column::Songs => {
                        if self.song_scroll >= a {
                            self.song_scroll -= a
                        } else {
                            self.song_scroll = 0
                        }
                    }
                }

                Ok(())
            }
            Action::MoveUp(a) => {
                match self.column {
                    Column::Artists => {
                        self.counter = self.column_timeout;

                        if self.artist_i >= a {
                            self.artist_i -= a
                        } else {
                            self.artist_i = 0
                        }
                    }
                    Column::Albums => {
                        self.counter = self.column_timeout;

                        if self.album_i >= a {
                            self.album_i -= a
                        } else {
                            self.album_i = 0
                        }
                    }
                    Column::Songs => {
                        if self.song_i >= a {
                            self.song_i -= a
                        } else {
                            self.song_i = 0
                        }
                    }
                }

                Ok(())
            }
            Action::ScrollDown(a) => {
                self.column_changed = true;

                if self.column == Column::Artists {
                    if self.artist_scroll + a + 1 < self.artists.len() {
                        self.artist_scroll += a;
                    } else {
                        self.artist_scroll = self.artists.len() - 1
                    }
                } else if self.column == Column::Albums {
                    if self.album_scroll + a + 1 < self.albums.len() {
                        self.album_scroll += a
                    } else {
                        self.album_scroll = self.albums.len() - 1
                    }
                } else if self.column == Column::Songs {
                    if self.song_scroll + a + 1 < self.songs.len() {
                        self.song_scroll += a
                    } else {
                        self.song_scroll = self.songs.len() - 1
                    }
                }

                Ok(())
            }
            Action::MoveDown(a) => {
                match self.column {
                    Column::Artists => {
                        self.counter = self.column_timeout;

                        if self.artist_i + a < self.artists.len() {
                            self.artist_i += a;
                        } else {
                            self.artist_i = self.artists.len();
                        }
                    }
                    Column::Albums => {
                        self.counter = self.column_timeout;

                        if self.album_i + a < self.albums.len() {
                            self.album_i += a
                        } else {
                            self.album_i = self.albums.len()
                        }
                    }
                    Column::Songs => {
                        if self.song_i + a < self.songs.len() {
                            self.song_i += a
                        } else {
                            self.song_i = self.songs.len()
                        }
                    }
                }

                Ok(())
            }
            Action::MoveRight => {
                if self.counter == 0 {
                    self.column_changed = true;

                    match self.column {
                        Column::Artists => self.column = Column::Albums,
                        Column::Albums => self.column = Column::Songs,
                        _ => (),
                    }
                }

                Ok(())
            }
            Action::MoveLeft => {
                self.column_changed = true;

                match self.column {
                    Column::Albums => self.column = Column::Artists,
                    Column::Songs => self.column = Column::Albums,
                    _ => (),
                }

                Ok(())
            }
            Action::AddToQueue => super::queue::add_to_queue(self.song.clone(), "song".into(), mpd),
            Action::RemoveFromQueue => {
                super::queue::remove_song_from_queue(&self.song.clone(), mpd)?;

                Ok(())
            }
            Action::AddOrRemoveFromQueue => {
                let song = self.song.clone();

                if let Some(n) = super::queue::song_in_queue(&song, mpd)? {
                    mpd.delete(n as u32)
                } else {
                    super::queue::add_to_queue(song, "song".into(), mpd)
                }
            }
            Action::AddToQueueAndPlay => {
                super::queue::add_to_queue_and_play(self.song.clone(), "song".into(), mpd)
            }
        }
    }

    // updates {2
    /// Update the current artist and optionally pull all artists from mpd
    fn update_artist(
        &mut self,
        mpd: &mut mpd::Client,
        refresh: bool,
    ) -> Result<bool, mpd::error::Error> {
        if self.artists.is_empty() || refresh {
            // get all artists
            self.artists = mpd.list(&Term::Tag("Artist".into()), &Query::new())?;

            self.artists.sort_by_key(|a| a.to_lowercase());
        }

        if !self.artists.is_empty() {
            // clamp artist_i
            if self.artist_i > self.artists.len() - 1 {
                self.artist_i = self.artists.len() - 1;
            }

            // check if artist changed
            if self.artist != self.artists[self.artist_i] {
                self.artist = self.artists[self.artist_i].clone();

                Ok(true)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }

    /// Update the current album and optionally pull all albums of current artist from mpd
    fn update_album(
        &mut self,
        mpd: &mut mpd::Client,
        refresh: bool,
    ) -> Result<bool, mpd::error::Error> {
        // refresh albums
        if refresh {
            self.albums = mpd.list(
                &Term::Tag("Album".into()),
                &Query::new().and(Term::Tag("Artist".into()), self.artist.clone()),
            )?;
        }

        if !self.albums.is_empty() {
            // clamp album_i
            if self.album_i > self.albums.len() - 1 {
                self.album_i = self.albums.len() - 1;
            }

            if self.album_scroll > self.albums.len() - 1 {
                self.album_scroll = self.albums.len() - 1;
            }

            // check if album changed
            if self.album != self.albums[self.album_i] {
                self.album = self.albums[self.album_i].clone();

                Ok(true)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }

    /// Update the current song and optionally pull all songs of current album from mpd
    fn update_song(
        &mut self,
        mpd: &mut mpd::Client,
        refresh: bool,
    ) -> Result<bool, mpd::error::Error> {
        // refresh songs
        if refresh {
            self.songs = mpd.find(
                &Query::new()
                    .and(Term::Tag("Album".into()), self.album.clone())
                    .and(Term::Tag("Artist".into()), self.artist.clone()),
                None,
            )?;
        }

        // clamp song_i
        if !self.songs.is_empty() {
            if self.song_i > self.songs.len() - 1 {
                self.song_i = self.songs.len() - 1;
            }

            if self.song_scroll > self.songs.len() - 1 {
                self.song_scroll = self.songs.len() - 1;
            }

            if self.song_i < self.song_scroll {
                self.song_i = self.song_scroll;
            }

            if self.song != self.songs[self.song_i] {
                self.song = self.songs[self.song_i].clone();

                Ok(true)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }

    /// Update everything
    pub fn update_all(
        &mut self,
        mpd: &mut mpd::Client,
    ) -> Result<(bool, bool, bool), mpd::error::Error> {
        Ok((
            self.update_artist(mpd, true)?,
            self.update_album(mpd, true)?,
            self.update_song(mpd, true)?,
        ))
    }
    // 2}

    /// Clear the column
    pub fn clear_column(
        &mut self,
        size: &mut super::display::Size,
        start: u16,
        col: u16,
    ) -> String {
        let mut out = String::new();

        for i in 8..(size.rows + 1) {
            out += &clear_section(start, i, col);
        }

        out
    }

    /// draw a column of LibraryItems
    pub fn draw_column(
        &mut self,
        size: &mut super::display::Size,
        start: u16,
        col: u16,
        scroll: usize,
        items: &[library_item::LibraryItem],
        clear: bool,
    ) -> String {
        let mut out = String::new();
        if clear {
            out += &self.clear_column(size, start, col);
        }

        for (index, item) in items.iter().enumerate() {
            if index < scroll {
                // Don't draw strings above of scroll
                continue;
            }

            if 8 + index > (size.rows as usize) + scroll {
                // stop when there is no more screen left
                break;
            }

            if item.selected() {
                out += &format!("{}", style::Invert);

                if !item.column_selected() {
                    out += &format!("{}", color::Fg(color::White));
                } else {
                    out += &format!("{}", color::Fg(color::Yellow));
                }
            } else {
                out += &format!("{}", color::Fg(color::Yellow));
            }

            if let Some(in_queue) = item.in_queue() {
                if in_queue {
                    out += &format!("{}", style::Bold);
                }
            }

            let mut name = item.name();
            if name.is_empty() {
                name = "<empty>".into();
            } else if name.len() > col as usize {
                name = name.split_at(col as usize - 4).0.to_string() + "...";
            };

            out += &format!(
                "{}{}{}",
                &goto_print(start, 8 + (index - scroll) as u16, name),
                style::NoInvert,
                style::NoBold,
            );
        }

        out
    }

    /// Get string to entire library
    pub fn draw(
        &mut self,
        mpd: &mut mpd::Client,
        size: &mut super::display::Size,
        queue_changed: bool,
        redraw_window: bool,
    ) -> Result<String, mpd::error::Error> {
        let mut out = String::new();

        self.cols.update(size);

        if redraw_window {
            out += &format!(
                "{}{}{}{}{}{}{}{}{}",
                color::Fg(color::Yellow),
                goto_print(1, 7, "─".repeat(size.cols as usize)),
                style::Bold,
                goto_print(1, 6, "Artists"),
                goto_print(self.cols.album_start, 6, "Albums"),
                goto_print(self.cols.song_start, 6, "Songs"),
                color::Fg(color::Reset),
                goto_print(size.half_cols - 2, 4, "Library"),
                style::NoBold,
            );
        }

        out += &format!("{}", color::Fg(color::Yellow));

        if self.update_artist(mpd, false)? || self.column_changed || redraw_window {
            out += &self.draw_column(
                size,
                1,
                self.cols.artist_cols,
                self.artist_scroll,
                &library_item::ArtistAlbumItem::from_vec(
                    self.artists.clone(),
                    self.artist_i,
                    self.column == Column::Artists,
                ),
                true,
            );
        }

        let album_changed =
            self.update_album(mpd, self.column == Column::Artists && self.counter == 1)?;

        if self.column == Column::Artists && self.counter == self.column_timeout {
            out += &self.clear_column(size, self.cols.album_start, self.cols.album_cols);
        } else if album_changed || redraw_window || self.column_changed || self.counter == 1 {
            out += &self.draw_column(
                size,
                self.cols.album_start,
                self.cols.album_cols,
                self.album_scroll,
                &library_item::ArtistAlbumItem::from_vec(
                    self.albums.clone(),
                    self.album_i,
                    self.column == Column::Albums,
                ),
                true,
            );
        }

        let song_changed =
            self.update_song(mpd, self.column != Column::Songs && self.counter == 1)?;

        if self.column != Column::Songs && self.counter == self.column_timeout {
            out += &self.clear_column(size, self.cols.song_start, self.cols.song_cols);
        } else if song_changed
            || redraw_window
            || (self.column_changed && self.column != Column::Artists)
            || self.counter == 1
            || queue_changed
        {
            let mut songs: Vec<library_item::LibraryItem> = Vec::new();

            for (i, song) in self.songs.clone().into_iter().enumerate() {
                let songfile = song.file.clone();

                let item = library_item::SongItem::new(
                    song,
                    i == self.song_i,
                    mpd.queue()?.iter().any(|s| s.file == songfile),
                    self.column == Column::Songs,
                );

                songs.push(library_item::LibraryItem::SongItem(item))
            }

            out += &self.draw_column(
                size,
                self.cols.song_start,
                self.cols.song_cols,
                self.song_scroll,
                &songs,
                true,
            );
        }

        self.column_changed = false;

        if self.counter > 0 {
            self.counter -= 1;
        }

        out += &format!("{}", color::Fg(color::Reset));

        Ok(out)
    }
}
