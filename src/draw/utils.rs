//! generally useful functions used for drawing
use termion::cursor::Goto;

/// Simple wrapper for clearing a section of screen
pub fn clear_section(start_col: u16, start_row: u16, len: u16) -> String {
    format!("{}{}", Goto(start_col, start_row), " ".repeat(len as usize))
}

/// Simple wrapper for quickly going to cell and printing a string
pub fn goto_print<S>(start_col: u16, start_row: u16, str: S) -> String
where
    S: std::fmt::Display,
{
    format!("{}{}", Goto(start_col, start_row), str)
}
