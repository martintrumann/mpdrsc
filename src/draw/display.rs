//! managment of screens, screen events and terminal size
use super::library::Action as LibraryAction;
use super::queue::Action as QueueAction;
use termion::event::Key;

/// All Screens
#[derive(PartialEq)]
pub enum Screen {
    Queue,
    Library,
}

/// Size of the terminal
pub struct Size {
    /// Number of columns
    pub cols: u16,
    /// The number of columns halved (used for centering)
    pub half_cols: u16,
    /// Number of rows
    pub rows: u16,
}

/// Main display struct
pub struct Display {
    /// Current size of the terminal
    pub size: Size,
    /// Current screen
    pub screen: Screen,
    /// If current screen changed
    pub screen_changed: bool,

    pub library: super::library::Library,
    pub queue: super::queue::Queue,
}

impl Display {
    /// Create new Display from screen size
    pub fn new(
        (cols, rows): (u16, u16),
        library: super::library::Library,
        queue: super::queue::Queue,
    ) -> Display {
        Display {
            size: Size {
                cols,
                rows,
                half_cols: cols / 2,
            },
            screen: Screen::Queue,
            screen_changed: false,

            library,
            queue,
        }
    }

    /// update the scree size
    pub fn update(&mut self, (cols, rows): (u16, u16)) -> bool {
        // reset screen_changed
        self.screen_changed = false;

        if cols != self.size.cols || rows != self.size.rows {
            self.size.cols = cols;
            self.size.half_cols = cols / 2;
            self.size.rows = rows;

            true
        } else {
            false
        }
    }

    /// Change the currently focused screen
    pub fn change_screen(&mut self, screen: Screen) {
        if self.screen != screen {
            self.screen = screen;

            self.screen_changed = true;
        } else {
            self.screen_changed = false;
        }
    }

    /// Send events to screens
    pub fn event(
        &mut self,
        event: termion::event::Key,
        mpd: &mut mpd::Client,
    ) -> Result<bool, mpd::error::Error> {
        if self.screen == Screen::Library {
            match event {
                Key::Char('h') => self.library.execute(LibraryAction::MoveLeft, mpd),
                Key::Char('l') => self.library.execute(LibraryAction::MoveRight, mpd),
                Key::Char('j') => {
                    self.library.execute(LibraryAction::MoveDown(1), mpd)?;
                    self.library.execute(LibraryAction::ScrollDown(1), mpd)
                }
                Key::Ctrl('j') => {
                    self.library.execute(LibraryAction::MoveDown(10), mpd)?;
                    self.library.execute(LibraryAction::ScrollDown(10), mpd)
                }
                Key::Char('k') => {
                    self.library.execute(LibraryAction::MoveUp(1), mpd)?;
                    self.library.execute(LibraryAction::ScrollUp(1), mpd)
                }
                Key::Ctrl('k') => {
                    self.library.execute(LibraryAction::MoveUp(10), mpd)?;
                    self.library.execute(LibraryAction::ScrollUp(10), mpd)
                }
                Key::Char(' ') => {
                    self.library
                        .execute(LibraryAction::AddOrRemoveFromQueue, mpd)?;
                    self.library.execute(LibraryAction::MoveDown(1), mpd)?;
                    self.library.execute(LibraryAction::ScrollDown(1), mpd)
                }
                Key::Ctrl('m') => self.library.execute(LibraryAction::AddToQueueAndPlay, mpd),
                _ => return Ok(false),
            }?
        } else if self.screen == Screen::Queue {
            match event {
                Key::Char('j') => self.queue.execute(QueueAction::MoveDown(1), mpd),
                Key::Char('k') => self.queue.execute(QueueAction::MoveUp(1), mpd),
                Key::Char('x') => self.queue.execute(QueueAction::RemoveSong(None), mpd),
                Key::Char('c') => self.queue.execute(QueueAction::Clear, mpd),
                _ => return Ok(false),
            }?;
        }

        Ok(true)
    }

    /// Draw the screen
    pub fn draw(
        &mut self,
        mpd: &mut mpd::Client,
        resized: bool,
    ) -> Result<String, mpd::error::Error> {
        let mut out = String::new();

        if self.screen_changed {
            out += &format!(
                "{}{}",
                termion::cursor::Goto(1, 6),
                termion::clear::AfterCursor
            );
        }

        let redraw = resized || self.screen_changed;

        self.queue.update(mpd)?;

        out += &match self.screen {
            Screen::Queue => self.queue.draw(mpd, &mut self.size, redraw),
            Screen::Library => {
                self.library
                    .draw(mpd, &mut self.size, self.queue.queue_changed, redraw)
            }
        }?;

        Ok(out)
    }
}
