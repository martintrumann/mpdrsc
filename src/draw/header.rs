//! Drawing function for the header
use mpd::State;
use termion::{clear, cursor::Goto, style};

use super::utils::{clear_section, goto_print};
use crate::song;
use crate::structs;

/// Get the header as a string
pub fn header(
    mpd_status: &mut structs::MpdStatus,
    display: &mut super::display::Display,
    resized: bool,
    song_changed: bool,
    status_changed: bool,
) -> String {
    let mut out = String::new();

    // check if song playing
    if resized || song_changed {
        if let Some(song) = &mpd_status.song {
            // redraw if song changed
            let song_info = song::info(&song);

            let song_name_draw_start = display.size.half_cols - 3 - (song_info.name_len / 2);
            let song_info_draw_start = display.size.half_cols + 1 - (song_info.info_str_len / 2);

            out += &format!(
                "{}{}{}{}{}{}",
                Goto(1, 1),
                clear::CurrentLine,
                goto_print(song_name_draw_start, 1, song_info.name),
                Goto(1, 2),
                clear::CurrentLine,
                goto_print(song_info_draw_start, 2, song_info.info_str),
            );
        } else {
            // clear song
            out += &format!(
                "{}{}{}{}",
                Goto(1, 1),
                clear::CurrentLine,
                Goto(1, 2),
                clear::CurrentLine,
            )
        }
    }

    // redraw duration or clear it if not playing
    if let Some((elapsed, duration)) = mpd_status.status.time {
        if status_changed {
            out += &format!(
                "{}{}{}{}",
                Goto(1, 1),
                style::Bold,
                song::elapsed_and_duration(elapsed, duration),
                style::NoBold,
            );
        }
    } else {
        out += &clear_section(1, 1, 14);
    }

    // redraw state if it changed
    if resized || status_changed {
        // redraw state
        out += &format!(
            "{}{}{}{}",
            style::Bold,
            Goto(1, 2),
            match mpd_status.status.state {
                State::Stop => "[stopped]",
                State::Play => "[playing]",
                State::Pause => "[paused] ",
            },
            style::NoBold
        );
    }

    // redraw volume if it changed
    if resized || status_changed {
        // resized volume
        out += &format!(
            "{}  Vol: {}%",
            Goto(
                display.size.cols - mpd_status.status.volume.to_string().len() as u16 - 7,
                1
            ),
            mpd_status.status.volume
        );
    }

    if resized {
        out += &format!(
            "{}{}{}{}",
            Goto(1, 3),
            "━".repeat(display.size.cols as usize),
            Goto(1, 5),
            "━".repeat(display.size.cols as usize)
        );
    }

    out
}
