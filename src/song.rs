//! Module to get information about song
use mpd::Song;
use std::time::Duration;
use termion::{color, style};

#[derive(Debug, Default)]
/// Songs name and info string and their length
pub struct SongInfo {
    /// songs name
    pub name: String,
    /// name strings length
    pub name_len: u16,
    /// info string (artist and album)
    pub info_str: String,
    /// info strings length
    pub info_str_len: u16,
}

/// get the songs info
pub fn info(song: &Song) -> SongInfo {
    let mut info_str = String::new();
    let mut info_str_len = 0;

    if let Some(artist) = &song.artist {
        info_str += &format!(
            "{}{}{}{}{}",
            color::Fg(color::Yellow),
            style::Bold,
            artist,
            style::NoBold,
            color::Fg(color::Reset),
        );

        info_str_len += artist.len();
    }

    let tags = &song.tags;
    if let Some(album) = tags.get("Album") {
        if !info_str.is_empty() {
            info_str += " - ";
            info_str_len += 3;
        }

        info_str += &format!(
            "{}{}{}",
            color::Fg(color::Cyan),
            album,
            color::Fg(color::Reset),
        );

        info_str_len += album.len();
    }

    if info_str.is_empty() {
        let mut file_arr = song.file.split('/');
        if let Some(dir) = file_arr.nth(file_arr.clone().count() - 2) {
            info_str += dir;
            info_str_len += dir.chars().count();
        }
    }

    let mut name = title_or_filename(&song);
    let name_len = name.chars().count();

    name = format!("━━┫ {}{}{} ┣━━", style::Bold, name, style::NoBold);

    SongInfo {
        name,
        name_len: name_len as u16,
        info_str,
        info_str_len: info_str_len as u16,
    }
}

/// get the songs title or filename if title doesn't exists
pub fn title_or_filename(song: &Song) -> String {
    if let Some(title) = &song.title {
        title.to_string()
    } else if let Some(filename) = song.file.split('/').last() {
        String::from(filename)
    } else {
        String::from("")
    }
}

/// get formated string for song duration and elapsed time
pub fn elapsed_and_duration(elapsed: Duration, duration: Duration) -> String {
    let mut elapsed_secs = elapsed.as_secs();
    let elapsed_mins = elapsed_secs / 60;
    elapsed_secs -= elapsed_mins * 60;

    let mut duration_secs = duration.as_secs();
    let duration_mins = duration_secs / 60;
    duration_secs -= duration_mins * 60;

    format!(
        "{}:{:02}/{}:{:02}",
        elapsed_mins, elapsed_secs, duration_mins, duration_secs
    )
}
