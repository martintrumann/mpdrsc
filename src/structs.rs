//! globaly used structs

/// current song and status
// TODO: seperate status and time for better redrawing
pub struct MpdStatus {
    pub song: Option<mpd::Song>,
    pub status: mpd::Status,
    pub last_status: mpd::Status,
    pub queue: Vec<mpd::Song>,

    pub song_changed: bool,
    pub status_changed: bool,
    pub queue_changed: bool,
}
impl MpdStatus {
    pub fn new(song: Option<mpd::Song>, status: mpd::Status, queue: Vec<mpd::Song>) -> MpdStatus {
        MpdStatus {
            song,
            status,
            last_status: mpd::status::Status::default(),
            queue,

            song_changed: false,
            status_changed: false,
            queue_changed: false,
        }
    }

    pub fn update(&mut self, song: Option<mpd::Song>, status: mpd::Status, queue: Vec<mpd::Song>) {
        if song != self.song {
            self.song = song;

            self.song_changed = true;
        } else {
            self.song_changed = false;
        }

        if status != self.status {
            self.last_status = self.status.clone();
            self.status = status;

            self.status_changed = true;
        } else {
            self.status_changed = false;
        }

        if queue != self.queue {
            self.queue = queue;

            self.queue_changed = true;
        } else {
            self.queue_changed = false;
        }
    }
}
